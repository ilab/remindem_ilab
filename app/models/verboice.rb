class Verboice

  def self.connect
    @@instance ||= Verboice.new(ENV['EMAIL'], ENV['PASSWORD'])
  end

  def initialize(email, token)
    @email = email
    @token = token
  end

  def call verboice_schedule, phone_number
    options = {
      channel_id: verboice_schedule.channel_id,
      call_flow_id: verboice_schedule.call_flow_id,
      address: phone_number
    }
    response = post("/call", {call: options})
    p response
    verboice_call = JSON.parse(response.body)
  end

  def call_logs params = {}
    verboice_query = auth_params(params.slice(:start_date, :end_date, :channel_id, :status)).to_query
    get("/call_logs?#{verboice_query}")
  end

  private

  def connect
    auth_url = build_url("/auth")
    response = Typhoeus.post(auth_url, body: { account: { email: @email, password: @password } })
    @token = JSON.parse(response.body)['auth_token']
  end

  def get(path)
    response = Typhoeus.get(build_url(path))
    JSON.parse(response.response_body)
  end

  def post(path, params)
    Typhoeus.post(build_url(path), body: JSON.generate(auth_params(params)), headers: {'content-type' => 'application/json'} )
  end

  def build_url(path)
    VerboiceConfig::Config["url"] + "/api2" + path
  end

  def auth_params(params)
    params.merge({ email: @email, token: @token })
  end
end