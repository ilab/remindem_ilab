# Copyright (C) 2011-2012, InSTEDD
# 
# This file is part of Remindem.
# 
# Remindem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Remindem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Remindem.  If not, see <http://www.gnu.org/licenses/>.

class VerboiceSchedule < ActiveRecord::Base
  validates_presence_of :email, :token, :project_id, :channel_id, :call_flow_id, :welcome_message, :timescale, :title, :keyword
  validates_presence_of :timescale, :unless => Proc.new {|schedule| schedule.type == "CalendarBasedSchedule"}
  validates_uniqueness_of :keyword
  validates_length_of :keyword, :maximum => 15
  validates_format_of :keyword, :with => /^[^ ]+$/, :message => "must not include spaces"
  validates_length_of :title, :maximum => 60
  has_one :schedule, :dependent => :destroy

end