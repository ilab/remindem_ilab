# Copyright (C) 2011-2012, InSTEDD
#
# This file is part of Remindem.
#
# Remindem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Remindem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Remindem.  If not, see <http://www.gnu.org/licenses/>.

class VerboiceSchedulesController < AuthenticatedController

  def initialize
    super
    @show_breadcrumb = true
    add_breadcrumb _("Reminders"), :schedules_path
  end

  # GET /schedules/1
  # GET /schedules/1.xml
  def show
    @schedule = VerboiceSchedule.find(params[:id])
    add_breadcrumb @schedule.title, verboice_schedule_path(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @schedule }
    end
  end

  def edit
    @schedule = VerboiceSchedule.find(params[:id])
    @schedule.type = @schedule.schedule.type
    add_breadcrumb @schedule.title, verboice_schedule_path(params[:id])
    add_breadcrumb _("Settings"), edit_schedule_path(params[:id])
  end

  def update
    @schedule = VerboiceSchedule.find(params[:id])
    type = params[:verboice_schedule][:type] || nil
    params[:verboice_schedule][:type] = nil
    respond_to do |format|
      if @schedule.update_attributes(params[:verboice_schedule])
        @reminder = @schedule.schedule
        @reminder.title = @schedule.title
        @reminder.keyword = @schedule.keyword
        @reminder.welcome_message = @schedule.welcome_message
        @reminder.timescale = @schedule.timescale
        @reminder.user = current_user
        @reminder.paused = @schedule.paused
        @reminder.type = type if type
        @reminder.verboice_schedule_id = @schedule.id
        @reminder.save!
        format.html { redirect_to(verboice_schedule_url(@schedule), :notice => _('Schedule was successfully updated.')) }
        format.xml  { head :ok }
      else
        add_breadcrumb @schedule.title, verboice_schedule_path(params[:id])
        add_breadcrumb _("Settings"), edit_verboice_schedule_path(params[:id])

        format.html { render :action => "edit" }
        format.xml  { render :xml => @schedule.errors, :status => :unprocessable_entity }
      end
    end
  end

  # GET /schedules/new
  # GET /schedules/new.xml
  def new
    add_breadcrumb _("New Reminder"), :new_verboice_schedule_path
    @schedule = VerboiceSchedule.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @schedule }
    end
  end

  def create
    @schedule = VerboiceSchedule.new(params[:verboice_schedule])
    # @schedule.type = params[:verboice_schedule][:type]
    respond_to do |format|
      if @schedule.save!
        @reminder = Schedule.new()
        @reminder.title = @schedule.title
        @reminder.keyword = @schedule.keyword
        @reminder.user = current_user
        @reminder.welcome_message = @schedule.welcome_message
        @reminder.timescale = @schedule.timescale
        @reminder.type = params[:verboice_schedule][:type]
        @reminder.verboice_schedule_id = @schedule.id
        @reminder.save!
        format.html { redirect_to(schedules_url(), :notice => _('Verboice Schedule was successfully created.')) }
        format.xml  { render :xml => @schedule, :status => :created, :location => @schedule }
      else
        add_breadcrumb _("New Reminder"), :new_verboice_schedule_path
        format.html { render :action => "new" }
        format.xml  { render :xml => @schedule.errors, :status => :unprocessable_entity }
      end
    end
  end

  def get_authentication_token
    url = VerboiceConfig::Config["url"] + "/api2/auth"
    variable = {'account[email]' => params[:email], 'account[password]' => params[:password]}
    res = request_verboice(url, variable, "post")
    if res.code == "200"
      render :json => res.body
    else
      render :text => "Error", :status => 500
    end
  end

  def get_verboice_projects
    url = VerboiceConfig::Config["url"] + "/api2/projects"
    variable = {'email' => params[:email], 'token' => params[:token]}
    res = request_verboice(url, variable, "get")
    if res.code == "200"
      render :text => res.body
    else
      render :text => "Error", :status => 500
    end
  end

  def get_verboice_callflows
    url = VerboiceConfig::Config["url"] + "/api2/call_flows"
    variable = {'email' => params[:email], 'token' => params[:token]}
    res = request_verboice(url, variable, "get")
    if res.code == "200"
      render :text => res.body
    else
      render :text => "Error", :status => 500
    end
  end

  def get_verboice_channels
    url = VerboiceConfig::Config["url"] + "/api2/channels"
    variable = {'email' => params[:email], 'token' => params[:token]}
    res = request_verboice(url, variable, "get")
    if res.code == "200"
      render :text => res.body
    else
      render :text => "Error", :status => 500
    end
  end

  private

  def request_verboice(url, variable, method)
    case method
    when "post"
      req = Net::HTTP::Post.new(url)
    when "get"
      req = Net::HTTP::Get.new(url)
    end
    uri = URI(url)
    req.set_form_data(variable)
    res = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
  end

end