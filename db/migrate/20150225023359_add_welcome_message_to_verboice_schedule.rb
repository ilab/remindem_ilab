class AddWelcomeMessageToVerboiceSchedule < ActiveRecord::Migration
  def self.up
  	add_column :verboice_schedules, :welcome_message, :string
  end

  def self.down
  	remove_column :verboice_schedules, :welcome_message
  end
end
