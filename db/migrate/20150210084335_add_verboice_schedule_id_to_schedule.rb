class AddVerboiceScheduleIdToSchedule < ActiveRecord::Migration
  def self.up
  	add_column :schedules, :verboice_schedule_id, :integer, :references => :verboice_schedules
  end

  def self.down
  	remove_column :schedules, :verboice_schedule_id
  end
end
