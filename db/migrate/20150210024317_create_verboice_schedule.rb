class CreateVerboiceSchedule < ActiveRecord::Migration
  def self.up
  	create_table :verboice_schedules do |t|
      t.string  :token
      t.string  :email
      t.integer :project_id
      t.integer :call_flow_id
      t.integer :channel_id
      t.string  :timescale
      t.boolean :random
      t.string  :type
      t.string  :title
      t.boolean :paused
      t.string  :keyword

      t.timestamps
    end
  end

  def self.down
  	drop_table :verboice_schedules
  end
end
