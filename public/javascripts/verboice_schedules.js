function VerboiceSchedules(data) {
  this.id = data.id;
	this.email = data.email;
	this.password = data.password;
	this.project_id = data.project_id;
	this.channel_id = data.channel_id;
	this.call_flow_id = data.call_flow_id;
  this.token = data.token;
  this.callflows = [];
  this.projects = [];
  this.channels = [];
}

VerboiceSchedules.prototype.authenticate = function() {
  var _this = this;
	jQuery.ajax({
    url: "/get_authentication_token",
    type: "GET",
    crossDomain: true,
    data: {"email" : this.email, "password" : this.password},
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    success: function(response) {
      _this.token = response["auth_token"];
      $("#verboice_schedule_token").val(_this.token)
      _this.query_callflows();
      _this.query_channels();
    },
    error: function(){
      $.status.showError("Failed connect to verboice", 500);
    }
  })
}

VerboiceSchedules.prototype.query_projects = function() {
  var _this = this;
  jQuery.ajax({
    url: "/get_verboice_projects",
    type: "GET",
    crossDomain: true,
    data: {"email" : this.email, "token" : this.token},
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    success: function(response) {
      _this.projects = response;
      _this.addProjects();
      if(_this.id != ""){
        _this.addCallFlow(_this.project_id);
        applySelectToCombo("verboice_schedule_call_flow_id", window.currentSchedule.callflows, $("#tmpCallFlowId").val());
        applySelectToCombo("verboice_schedule_project_id", window.currentSchedule.projects, $("#tmpProjectId").val());
      }
      else{
        _this.addCallFlow(_this.projects[0]["id"]);
      }
      $.status.showNotice("Successfully connect to verboice", 500); 
    }
  })
}

VerboiceSchedules.prototype.query_callflows = function() {
  var _this = this;
  jQuery.ajax({
    url: "/get_verboice_callflows",
    type: "GET",
    crossDomain: true,
    data: {"email" : this.email, "token" : this.token},
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    success: function(response) {
      _this.callflows = response;
      _this.query_projects();
    }
  })
}

VerboiceSchedules.prototype.query_channels = function() {
  var _this = this;
  jQuery.ajax({
    url: "/get_verboice_channels",
    type: "GET",
    crossDomain: true,
    data: {"email" : this.email, "token" : this.token},
    dataType: 'json',
    contentType: "application/json; charset=utf-8",
    success: function(response) {
      _this.channels = response;
      clearSelect('verboice_schedule_channel_id');
      addListToSelect('verboice_schedule_channel_id', _this.channels);
      if(_this.id != ""){
        applySelectToCombo("verboice_schedule_channel_id", window.currentSchedule.channels, $("#tmpChannelId").val())
      }
    }
  })
}

VerboiceSchedules.prototype.addProjects = function(){
  clearSelect("verboice_schedule_project_id");
  addListToSelect('verboice_schedule_project_id', this.projects);
}

VerboiceSchedules.prototype.addCallFlow = function(projectId){
  clearSelect('verboice_schedule_call_flow_id');
  for(var j=0; j< this.callflows.length; j++){
    if(this.callflows[j]["project_id"].toString() == projectId.toString()){
      appendOptionToSelect("verboice_schedule_call_flow_id", this.callflows[j]["id"], this.callflows[j]["name"]);
    }
  }
}

function addListToSelect(id, list){
  for(var j=0; j< list.length; j++){
    $('#'+id)
    .append($("<option></option>")
    .attr("value",list[j]["id"])
    .text(list[j]["name"]));
  }
}

function appendOptionToSelect(id, value, text){
  $('#'+id)
    .append($("<option></option>")
    .attr("value",value)
    .text(text));
}

function clearSelect(id){
  $('#' + id)
  .find('option')
  .remove();
}

function authenticate_verboice(){
  data = get_verboice_form_data();
	window.currentSchedule = new VerboiceSchedules(data);
	window.currentSchedule.authenticate();
}

function get_verboice_form_data(){
  data = {};
  data["id"] = $("#tmpId").val();
  data["password"] = $("#password").val();
  data["email"] = $("#verboice_schedule_email").val();
  data["project_id"] = $("#verboice_schedule_project_id").val();
  data["channel_id"] = $("#verboice_schedule_channel_id").val();
  data["call_flow_id"] = $("#verboice_schedule_call_flow_id").val();
  data["token"] = $("#verboice_schedule_token").val();
  return data;
}

function changeProject(el){
  clearSelect("verboice_schedule_call_flow_id");
  for(var j=0; j< window.currentSchedule.callflows.length; j++){
    if(window.currentSchedule.callflows[j]["project_id"].toString() == el.value.toString()){
      appendOptionToSelect("verboice_schedule_call_flow_id", window.currentSchedule.callflows[j]["id"], window.currentSchedule.callflows[j]["name"]);
    }
  }
}

function connectVerboice(){
  $("#password").val("**********");
  data = get_verboice_form_data();
  data["project_id"] = $("#tmpProjectId").val();
  window.currentSchedule = new VerboiceSchedules(data);
  window.currentSchedule.query_callflows();
  window.currentSchedule.query_channels();
}

function applySelectToCombo(id, list, item){
  for(var i=0; i< list.length; i++){
    if(list[i].id == item){
      $("#" + id).val(item);
    }
  }
}



